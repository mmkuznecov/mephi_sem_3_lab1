#include "gtest/gtest.h"
#include "../ListSequence.hpp"
#include "../ArraySequence.hpp"
#include "../Sorts.hpp"

using namespace Sorts;

int int_arr[] = {4, 323, -999, 8, 5};
float float_arr[] = {1.112, 5.3, 1.111, 2.5, 7.9, 525.0, -76.5};

int *arr_int = int_arr;
float *float_int = float_arr;


template<typename ArrayType, template<typename> class SeqType>
Sequence<ArrayType>* CreateSeqFromArray(ArrayType *array, int arr_len){
    Sequence<ArrayType> *seq;
    auto help_seq = new SeqType<ArrayType>(array, arr_len);
    seq = help_seq;
    return seq;
}

template <typename T>
bool IsSorted(Sequence<T>* seq){

    int size = seq->GetLength();
    int i = 1;
    while ( i + 1 <= size)
    {
        if (seq->Get(i) >= seq->Get(i - 1)){i++;}
        else{return false;}
    }
    return true;
}

TEST(Sorts, ArraySelectionSort)
{

    auto seq = CreateSeqFromArray<int, ListSequence>(arr_int, 5);
    SelectionSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}

TEST(Sorts, ArraytHeapSort)
{
    auto seq = CreateSeqFromArray<int, ArraySequence>(arr_int, 5);
    HeapSort(seq);
    ASSERT_EQ(IsSorted(seq), true);


}

TEST(Sorts, ArrayShellSort)
{
    auto seq = CreateSeqFromArray<int, ArraySequence>(arr_int, 5);
    ShellSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}

TEST(Sorts, DoubleArraySelectionSort)
{
    auto seq = CreateSeqFromArray<float, ArraySequence>(float_int, 7);
    SelectionSort(seq);
    ASSERT_EQ(IsSorted(seq), true);
}

TEST(Sorts, DoubleArrayHeapSort)
{
    auto seq = CreateSeqFromArray<float, ArraySequence>(float_int, 7);
    HeapSort(seq);
    ASSERT_EQ(IsSorted(seq), true);
}

TEST(Sorts, DoubleArrayShellSort)
{
    Sequence<float>* seq;
    auto seq_help = new ArraySequence<float>(float_int, 7);
    seq = seq_help;
    ShellSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}

TEST(Sorts, ListSelectionSort)
{
    Sequence<int>* seq;
    auto seq_help = new ListSequence<int>(arr_int, 5);
    seq = seq_help;
    SelectionSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}

TEST(Sorts, ListtHeapSort)
{
    Sequence<int>* seq;
    auto seq_help = new ListSequence<int>(arr_int, 5);
    seq = seq_help;
    HeapSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}

TEST(Sorts, ListShellSort)
{
    Sequence<int>* seq;
    auto seq_help = new ListSequence<int>(arr_int, 5);
    seq = seq_help;
    ShellSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}

TEST(Sorts, DoubleListSelectionSort)
{
    Sequence<float>* seq;
    auto seq_help = new ListSequence<float>(float_int, 7);
    seq = seq_help;
    SelectionSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}

TEST(Sorts, DoubleListHeapSort)
{
    Sequence<float>* seq;
    auto seq_help = new ListSequence<float>(float_int, 7);
    seq = seq_help;
    HeapSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}

TEST(Sorts, DoubleListShellSort)
{
    Sequence<float>* seq;
    auto seq_help = new ListSequence<float>(float_int, 7);
    seq = seq_help;
    ShellSort(seq);
    ASSERT_EQ(IsSorted(seq), true);

}
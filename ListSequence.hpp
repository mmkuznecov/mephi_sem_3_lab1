#pragma once

#include "Sequence.hpp"
#include "LinkedList.hpp"

template <class T>
class ListSequence : public Sequence<T>{

private:
    LinkedList<T>* linked_list;

public:

    ListSequence()
    {
        this->linked_list = new LinkedList<T>();
    }
    ListSequence(int size)
    {
        this->linked_list = new LinkedList<T>(size);

    }
    ListSequence(T* item, int size)
    {
        this->linked_list = new LinkedList<T>(item, size);
    }
    ListSequence(LinkedList<T>* list)
    {
        this->linked_list = list;
    }
    ListSequence(const ListSequence<T>& seq)
    {
        this->linked_list = new LinkedList<T>(*seq.linked_list);
    }

    //DECOMPOSITION
    int GetLength() const override
    {
        return this->linked_list->GetLength();
    }

    T Get(int index) const override
    {
        if (index<0)
        {
            throw out_of_range(INDEX_OUT_OF_RANGE);
        }
        return this->linked_list->Get(index);
    }

    T GetFirst() const override
    {
        if (this->GetLength()==0)
        {
            throw out_of_range(INDEX_OUT_OF_RANGE);
        }
        return this->linked_list->GetFirst();
    }

    T GetLast() const override
    {
        if (this->GetLength()==0)
        {
            throw out_of_range(INDEX_OUT_OF_RANGE);
        }
        return this->linked_list->GetLast();
    }

    ListSequence<T>* GetSubSequence(int start, int end) override
    {
        if (start<0 || start>=this->GetLength() || end<0 || end >=this->GetLength() || end<start)
        {
            throw out_of_range(INDEX_OUT_OF_RANGE);
        }
        LinkedList<T>* list = new LinkedList<T>();

        list = this->linked_list->GetSublist(start, end);

        ListSequence<T>* listSequence =new ListSequence<T>(list);

        return listSequence;
    }


    //OPERATIONS

    void Set(int index, T item) override
    {
        if ((index<0) || index>this->GetLength())
        {
            throw out_of_range(INDEX_OUT_OF_RANGE);
        }
        return this->linked_list->Set(index, item);
    }

    void Prepend(T item) override
    {
        this->linked_list->Prepend(item);
    }

    void Append(T item) override
    {
        this->linked_list->Append(item);
    }

    void InsertAt(T item, int index) override
    {
        this->linked_list->InsertAt(item, index);
    }

     ListSequence<T>* Concat(Sequence<T>* list) override
     {
        ListSequence<T>* newlinkedlist = new ListSequence<T>;
        for (int i = 0; i < this->GetLength(); i++)
        {
            newlinkedlist->Append(this->Get(i));
        }

        for (int i = 0; i < list->GetLength(); i++)
        {
            newlinkedlist->Append(list->Get(i));
        }
        return newlinkedlist;
    }

    void PrettyPrint() override{
        this->linked_list->PrettyPrint();
    }


    ~ListSequence() = default;
};
#ifndef LAB1_SORTS_HPP
#define LAB1_SORTS_HPP


#include "Sequence.hpp"
#include "ArraySequence.hpp"
#include <ctime>

namespace Sorts{

    template<typename T>
    void Swap(Sequence<T> *seq, int i, int j){
        auto temp = seq->Get(i);
        seq->Set(i, seq->Get(j));
        seq->Set(j, temp);
    }

    template<typename T>
    int Compare(T item1, T item2){
        if (item1 > item2){return 1;}
        else if(item1 == item2){return 0;}
        else{return -1;}
    }


    template<class T>
    void SelectionSort(Sequence<T> *seq, int (*comp)(T item1, T item2))
    {
        int size = seq->GetLength();
        T temp;
        int j;

        for (int i = 0; i < size ; i++)
        {
            j = i;
            for(int k = i; k < size; k++)
            {
                //if(seq->Get(j) > seq->Get(k))
                if (comp(seq->Get(j), seq->Get(k)) == 1)
                {
                    j = k;
                }
            }

            Swap(seq, i, j);

        }
    }

    template <typename T>
    void Heapify(Sequence<T> *seq, int n, int ind, int (*comp)(T item1, T item2)){

        int largest = ind;
        int l = 2 * ind + 1;
        int r = 2 * ind + 2;

        if (l < n && comp(seq->Get(l), seq->Get(largest)) == 1){ largest = l;}
        if (r < n && comp(seq->Get(r), seq->Get(largest)) == 1){ largest = r;}
        if (largest != ind){
            Swap(seq, ind, largest);
            Heapify(seq, n, largest, comp);
        }

    }

    template<typename T>
    void HeapSort(Sequence<T> *seq, int (*comp)(T item1, T item2)){

        int n = seq -> GetLength();

        for (int i = n / 2 - 1; i >= 0; i--){
            Heapify(seq, n, i, comp);
        }

        for (int i = n - 1; i > 0; i--){
            Swap(seq, 0, i);
            Heapify(seq, i, 0, comp);
        }
    }


    template<typename T>
    void ShellSort(Sequence<T> *seq, int (*comp)(T item1, T item2))
    {
        int size = seq->GetLength();
        int d = size / 2;
        T temp;
        while (d > 0) {
            for (int i = 0; i < size - d; i++) {
                int j = i;
                while (j >= 0 && comp(seq->Get(j), seq->Get(j + d)) == 1) {
                    Swap(seq, j, j+d);
                    j--;
                }
            }
            d /= 2;
        }
    }

}

#endif // LAB1_SORTS_HPP
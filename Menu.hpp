#ifndef LAB1_MENU_HPP
#define LAB1_MENU_HPP

#include <iostream>
#include "Sequence.hpp"
#include "ArraySequence.hpp"
#include "ListSequence.hpp"
#include "Sorts.hpp"
#include <string>
#include <ctime>
#include <random>

std::random_device rd;
std::mt19937 mt(rd());
std::uniform_real_distribution<float> dist(-1000.0, 1000.0);

using namespace std;
using namespace Sorts;

string alg_options[3] = {"1--ShellSort", "2--HeapSort", "3--SelectionSort"};
string type_options[2] = {"1--int", "2--float"};
string type_sequence[2] = {"1--ListSequence", "2--ArraySequence"};
string main_menu_options[4] = {"1--Automatic fill", "2--Manual fill", "3--Compare Algs", "4--Exit"};


double GetTime(clock_t StartTime, clock_t EndTime){
    return (double) (EndTime - StartTime) / CLOCKS_PER_SEC;
}



int GetOption(string MSGS[], int len){

    cout << "=======================" << endl;

    for (int i = 0; i < len; i++){
        cout << MSGS[i] << endl;
    }

    cout << "=======================" << endl;

    int choice;

    cout << "Enter your choice: ";
    cin >> choice;

    return choice;
}

template<typename ArrayType, template<typename> class SeqType>
Sequence<ArrayType>* CreateSeq(int seq_size){
    auto seq = new SeqType<ArrayType>(seq_size);

    for (int i = 0; i < seq_size; i++){
        ArrayType item = dist(mt);
        seq->Set(i, item);
    }

    return seq;
}

template<typename ArrayType, template<typename> class SeqType>
Sequence<ArrayType>* ManualEnterSequence(int seq_size){
    auto seq = new SeqType<ArrayType>(seq_size);
    cout << "Enter " << seq_size << " numbers:" << endl;

    for (int i = 0; i < seq_size; i++)
    {
        ArrayType item;
        cout << "Your number: ";
        cin >> item;
        cout << endl;
        seq->Set(i, item);
    }
    return seq;

}

template<typename ArrayType, template<typename> class SeqType>
Sequence <ArrayType>* DuplicateSeq(Sequence<ArrayType> *seq){

    int size = seq->GetLength();
    auto new_seq = new SeqType<ArrayType>(size);

    for (int i = 0; i < size; i++){
        ArrayType item = seq->Get(i);
        new_seq->Set(i, item);
    }

    return new_seq;

}

template<typename ArrayType, template<typename> class SeqType>
void MenuBlock(int size, int exec_option, void (*sort)(Sequence<ArrayType> *sequence, int (*comp)(ArrayType item1, ArrayType item2))){
    Sequence<ArrayType>* seq;
    if (exec_option - 1) { seq = ManualEnterSequence<ArrayType, SeqType>(size); }
    else {seq = CreateSeq<ArrayType, SeqType>(size); }
    cout << "Before sort" << endl;
    seq->PrettyPrint();
    clock_t Start = clock();
    sort(seq, Compare);
    clock_t End = clock();
    cout << "After sort" << endl;
    seq->PrettyPrint();
    cout << "Time required: " << GetTime(Start, End) << "sec" << endl;
}

void RunMenu(int* choices, int size, int exec_option){


    switch (choices[0]) { // algo


        case 1:
            switch (choices[1]) { // value type
                case 1:
                    if (choices[2]-1){MenuBlock<int, ArraySequence>(size, exec_option, ShellSort);}
                    else{MenuBlock<int, ListSequence>(size, exec_option, ShellSort);}
                    break;
                case 2:
                    if (choices[2]-1){MenuBlock<float, ArraySequence>(size, exec_option, ShellSort);}
                    else{MenuBlock<float, ListSequence>(size, exec_option, ShellSort);}
                    break;
            }
            break;
        case 2:
            switch (choices[1]) { // value type
                case 1:
                    if (choices[2]-1){MenuBlock<int, ArraySequence>(size, exec_option, HeapSort);}
                    else{MenuBlock<int, ListSequence>(size, exec_option, HeapSort);}
                    break;
                case 2:
                    if (choices[2]-1){MenuBlock<float, ArraySequence>(size, exec_option, HeapSort);}
                    else{MenuBlock<float, ListSequence>(size, exec_option, HeapSort);}
                    break;
            }
            break;
        case 3:
            switch (choices[1]) { // value type
                case 1:
                    if (choices[2]-1){MenuBlock<int, ArraySequence>(size, exec_option, SelectionSort);}
                    else{MenuBlock<int, ListSequence>(size, exec_option, SelectionSort);}
                    break;
                case 2:
                    if (choices[2]-1){MenuBlock<float, ArraySequence>(size, exec_option, SelectionSort);}
                    else{MenuBlock<float, ListSequence>(size, exec_option, SelectionSort);}
                    break;
            }
            break;
    }
}

template <typename T>
double GetTime(Sequence<T> *seq, void (*sort)(Sequence<T> *sequence, int (*comp)(T item1, T item2))){
    clock_t Start = clock();
    sort(seq, Compare);
    clock_t End = clock();
    return (double) (End - Start) / CLOCKS_PER_SEC;
}


template <typename ArrayType, template<typename> class SeqType>
void ShowComparisonResults(int seq_size){

    auto *seq = CreateSeq<ArrayType, SeqType>(seq_size);

    auto *seq_SS = DuplicateSeq<ArrayType, SeqType>(seq);
    auto *seq_SHS = DuplicateSeq<ArrayType, SeqType>(seq);
    auto *seq_HS = DuplicateSeq<ArrayType, SeqType>(seq);

    double SS_time = GetTime(seq_SS, SelectionSort);
    double SHS_time = GetTime(seq_SHS, ShellSort);
    double HS_time = GetTime(seq_HS, HeapSort);

    cout << "Comparison Results: " << endl;

    cout << "Selection Sort: " << SS_time << endl;
    cout << "Shell Sort: " << SHS_time << endl;
    cout << "Heap Sort: " << HS_time << endl;
}


void RunComparison(int seq_size){

    int type_choice = GetOption(type_options, 2);
    int seq_choice = GetOption(type_sequence, 2);

    switch (type_choice) {
        case 1:
            switch (seq_choice) {
                case 1:
                    ShowComparisonResults<int, ListSequence>(seq_size);
                    break;
                case 2:
                    ShowComparisonResults<int, ArraySequence>(seq_size);
                    break;
            }
            break;
        case 2:
            switch (seq_choice) {
                case 1:
                    ShowComparisonResults<float, ListSequence>(seq_size);
                    break;
                case 2:
                    ShowComparisonResults<float, ArraySequence>(seq_size);
                    break;
            }
            break;
    }
}

void Run(){
    int exec_option = GetOption(main_menu_options, 4);

    int alg_choice;
    int type_choice;
    int seq_choice;
    int choices[3];

    int size;

    while(exec_option != 4){

        cout << "Enter size of the sequence: ";
        cin >> size;

        if (exec_option == 3){ RunComparison(size);}
        else {
            alg_choice = GetOption(alg_options, 3);
            type_choice = GetOption(type_options, 2);
            seq_choice = GetOption(type_sequence, 2);
            choices[0] = alg_choice; choices[1] = type_choice; choices[2] = seq_choice;

            RunMenu(choices, size, exec_option);
        }

        exec_option = GetOption(main_menu_options, 4);
    }
}


#endif //LAB1_MENU_HPP
